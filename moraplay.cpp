#include <utility>
#include <vector>
#include <string>
#include <eosiolib/eosio.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/dispatcher.hpp>
#include <eosiolib/multi_index.hpp>

using namespace eosio;

class moraplay : public eosio::contract
{
  private:
    //@abi table accounts i64

  public:
    using contract::contract;

    /// @abi action
    void hi(account_name user)
    {
        require_auth(user);
        print("Hello, ", name{user});
    }
    //@abi action
    void deposit(const account_name from, const asset &quantity)
    {
        require_auth(from);
        eosio_assert(quantity.is_valid(), "invalid quantity");
        eosio_assert(quantity.amount > 0, "must deposit positive quantity");

        action(
            permission_level{from, N(active)},
            N(eosio.token), N(transfer),
            std::make_tuple(from, _self, quantity, std::string("deposit")))
            .send();
    }

    //@abi action
    void withdraw(const account_name to, const asset &quantity)
    {
        require_auth(_self);
        eosio_assert(quantity.is_valid(), "invalid quantity");
        eosio_assert(quantity.amount > 0, "must deposit positive quantity");

        action(
            permission_level{_self, N(active)},
            N(eosio.token), N(transfer),
            std::make_tuple(_self, to, quantity, std::string("withdraw")))
            .send();
    }
};

EOSIO_ABI(moraplay, (hi)(deposit)(withdraw))