#include <eosiolib/eosio.hpp>
#include <eosiolib/token.hpp>

using namespace eosio;

class justcontract : public eosio::token
{
private:
  

public:
  justcontract( account_name self):token(self){

  }
  /// @abi action
  void hi(account_name user)
  {
    require_auth(user);
    print("Hello, ", name{user});
  }
};

EOSIO_ABI(justcontract, (hi))